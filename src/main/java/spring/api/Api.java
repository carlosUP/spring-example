package spring.api;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@RestController
public class Api {

    private HashMap<Integer,User> database = new HashMap<>();

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> user(@PathVariable("id") String id) {

        User user;
        JSONObject json = new JSONObject();

        try {
            user = database.get(Integer.parseInt(id));
        } catch (NumberFormatException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error("'id' must be an integer"));
        }

        if (user==null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error("user with id "+id+" does not exist"));

        json.put("id",user.getId()).put("name",user.getName());
        return ResponseEntity.status(HttpStatus.OK).body(json.toString());

    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<String> users() {

        User user;
        JSONObject json;
        JSONArray array = new JSONArray();

        for (int key: database.keySet()) {
            user = database.get(key);
            json = new JSONObject().put("id",user.getId()).put("name",user.getName());
            array.put(json);
        }

        return ResponseEntity.status(HttpStatus.OK).body(array.toString());

    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<String> addUser(@RequestBody Map<String,String> body) {

        User user = new User();
        JSONObject json = new JSONObject();

        if (!body.containsKey("id"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error("missing field 'id'"));

        if (!body.containsKey("name"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error("missing field 'name'"));

        try {
            user.setId(Integer.parseInt(body.get("id")));
            user.setName(body.get("name"));
        } catch (NumberFormatException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error("'id' must be an integer"));
        }

        database.put(user.getId(), user);
        json.put("path", "/users/"+user.getId());
        return ResponseEntity.status(HttpStatus.CREATED).body(json.toString());

    }

    private String error(String message) {
        return new JSONObject().put("error",message).toString();
    }

}
